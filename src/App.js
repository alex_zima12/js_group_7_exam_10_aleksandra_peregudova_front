import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Layout from "./components/Layout/Layout";
import AddNewPost from "./containers/AddNewPost/AddNewPost";
import NewsList from "./containers/NewsList/NewsList";
import FullPost from "./components/FullPost/FullPost";

const App = () => (
    <Layout>
      <Switch>
        <Route path="/" exact component={NewsList}/>
        <Route path="/news/add" exact component={AddNewPost}/>
        <Route path="/news/:id" exact component={FullPost}/>
        <Route render={() => <h1>404 Not Found</h1>}/>
      </Switch>
    </Layout>
);

export default App;
