import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {apiURL} from "../../constans";
import {fetchComments} from "../../store/actions/actionsComments";
import Comment from "../Comment/Comment";
import AddCommentForm from "../AddCommentForm/AddCommentForm";

const FullPostInfo = ({title, date, image,description,id}) => {
    const dispatch = useDispatch();
    const comments = useSelector(state => state.comments.comments);

    useEffect(() => {
        dispatch(fetchComments(id));
    }, [dispatch,id]);

    let cardImage;
    if (image) {
        cardImage = <img src={apiURL + "/uploads/" + image} className="img-thumbnail" alt="to this Post"/>
    }

    let arrComments = [];
    comments.map((comment)=> {
        let newComment = <Comment
            id={comment.id}
            key={comment.id}
            author={comment.author}
            comment={comment.commentar}/>;
        return arrComments.push(newComment)
    });


    return (
        <div className="m-3">
        <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert">
            <div className="toast-header">
                <strong className="mr-auto">{title}</strong>
            </div>
            <p>
                {date}
            </p>
            <p>
                {description}
            </p>
            {cardImage}
        </div>
            {arrComments}
            <AddCommentForm/>
            </div>
    );
};

export default FullPostInfo;