import React from 'react';
import {apiURL} from "../../constans";
import {useDispatch} from "react-redux";
import {removePost} from "../../store/actions/actionsNews";
import {NavLink} from "react-router-dom";

const Post = (props) => {
    const dispatch = useDispatch();

    let cardImage;
    if (props.image) {
        cardImage = <img src={apiURL + "/uploads/" + props.image} className="img-thumbnail" alt="to this Post"/>
    }

   const removePostHandler = (id) => {
       dispatch(removePost(id))}

    return (
        <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert">
            <div className="toast-header">
                <strong
                    className="mr-auto"
                >{props.title}
                </strong>
            </div>
            <p>
                {props.date}
            </p>
            {cardImage}
            <button
                type="button"
                className="btn btn-danger m-3"
                onClick={ removePostHandler(props.id)}
            >Delete</button>
            <NavLink
                className="btn btn-success m-3"
                to={"/news/" + props.id}
                exact
            >Read more
            </NavLink>
        </div>
    );
};

export default Post;