import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchSelectedPost} from "../../store/actions/actionsNews";
import FullPostInfo from "../FullPostInfo/FullPostInfo";

const FullPost = props => {
    const dispatch = useDispatch();
    const post = useSelector(state => state.news.post);
    console.log(post,"post");

    useEffect(() => {
        dispatch(fetchSelectedPost(props.match.params.id));
    }, [dispatch,props.match.params.id]);


    return post && (
        <div className="container">
            <FullPostInfo
                id={post.id}
                key={post.id}
                title={post.news_title}
                image={post.news_image}
                description={post.news_description}
                date={post.news_date}/>;
        </div>
    );
};

export default FullPost;