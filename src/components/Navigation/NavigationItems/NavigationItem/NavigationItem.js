import React from 'react';
import {NavLink} from "react-router-dom";

const NavigationItem = ({to, exact, children}) => (
    <li className="nav-item">
        <NavLink className="nav-link text-white font-weight-bold" to={to} exact={exact}>{children}</NavLink>
    </li>
);

export default NavigationItem;