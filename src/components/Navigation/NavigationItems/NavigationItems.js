import React from 'react';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="nav justify-content-end">
            <NavigationItem to="news/add" exact>Add new contact</NavigationItem>
        </ul>
    );
};

export default NavigationItems;