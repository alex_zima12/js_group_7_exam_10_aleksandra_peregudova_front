import React from "react";
import {removeComment} from "../../store/actions/actionsComments";
import {useDispatch} from "react-redux";

const Comment = ({author,comment,id}) => {
    const dispatch = useDispatch();

    const removeCommentHandler = (id) =>
    {dispatch(removeComment(id));}

    return (
        <div aria-live="assertive" aria-atomic="true" className="alert alert-secondary" role="alert">
            <div className="toast-header">
                <strong className="mr-auto">{author}</strong>
            </div>
            <p>
                {comment}
            </p>
            <button
                type="button"
                className="btn btn-danger"
                onClick={(removeCommentHandler(id))}
            >Delete</button>
        </div>
    );
};

export default Comment;