import React, { useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {sendComment} from "../../store/actions/actionsComments";
import {NavLink} from "react-router-dom";

const AddCommentForm = () => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.comments.error);
    const [state, setState] = useState({
        news_id:"",
        author: "",
        commentar: ""
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

      const submitFormHandler = e => {
        e.preventDefault();
        const obj = {
            news_id: state.news_id,
            author: state.author,
            commentar:state.commentar
        }
          dispatch(sendComment(obj));
    };

    let errorMessage = '';
    if (error) {
        errorMessage = <h1>{error}</h1>
        return errorMessage;
    }

    return (
        <div className="container bg-gradient-info">
            {errorMessage}
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">ID</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="news_id"
                           placeholder="News ID"
                           onChange={inputChangeHandler}
                           name="news_id"
                           required
                    />

                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Author</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="author"
                           placeholder="Enter your name"
                           onChange={inputChangeHandler}
                           name="Author"
                    />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Comment</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="Enter comment"
                              onChange={inputChangeHandler}
                              name="commentar"
                              required
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Save</button>
            </form>
            <NavLink
                className="btn btn-success m-3"
                to={"/"}
                exact
            >Back to all news
            </NavLink>
        </div>
    );
};

export default AddCommentForm;