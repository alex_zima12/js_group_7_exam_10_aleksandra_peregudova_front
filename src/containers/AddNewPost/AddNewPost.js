import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {sendPost} from "../../store/actions/actionsNews";

const AddNewPost = (props) => {
    const dispatch = useDispatch();
    // const news = useSelector(state => state.news);
    const error = useSelector(state => state.news.error);
    const [state, setState] = useState({
        news_title: "",
        news_description: "",
        news_image: ""
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];
        setState(prevState => ({...prevState, [name]: file}));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const formData = new FormData();
        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });
        dispatch(sendPost(formData));
        props.history.push("/");
    };

    let errorMessage = '';
    if (error) {
        errorMessage = <h1>{error}</h1>
        return errorMessage;
    }
    return (
        <div className="container bg-gradient-info">
            {errorMessage}
            {/*{arr}*/}
            <form
                className="p-5"
                onSubmit={submitFormHandler}
            >
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="basic-addon1">Title</span>
                    </div>
                    <input type="text"
                           className="form-control"
                           id="title"
                           placeholder="News title"
                           onChange={inputChangeHandler}
                           name="news_title"
                           required/>

                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text">Content</span>
                    </div>
                    <textarea className="form-control"
                              placeholder="Enter news"
                              onChange={inputChangeHandler}
                              name="news_description"
                              required
                    />
                </div>
                <div className="input-group mb-3 ">
                    <label htmlFor="FormControlFile1">Add file</label>
                    <input
                        type="file"
                        name="news_image"
                        className="form-control-file"
                        id="FormControlFile1"
                        onChange={fileChangeHandler}
                    />
                </div>
                <button
                    type="submit"
                    className="btn btn-info"
                >Save</button>
            </form>
        </div>
    );
};

export default AddNewPost;