import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchNews} from "../../store/actions/actionsNews";
import Post from "../../components/Post/Post";

const NewsList = () => {
    const dispatch = useDispatch();
    const news = useSelector(state => state.news.news);

    useEffect(() => {
        dispatch(fetchNews());
    }, [dispatch,news]);

    let arr = [];
    news.map((post)=> {
        let newPost = <Post
            id = {post.id}
            key={post.id}
            title={post.news_title}
            image={post.news_image}
            date={post.news_date}/>;
        return arr.push(newPost)
    });

    // let errorMessage = '';
    // if (error) {
    //     errorMessage = <h1>{error}</h1>
    //     return errorMessage;
    // }

    return (
        <div className="container bg-gradient-info mt-3">
            {/*{errorMessage}*/}
            {arr}
        </div>
    );
};

export default NewsList;