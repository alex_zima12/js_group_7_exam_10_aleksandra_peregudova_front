import {
    FETCH_NEWS_SUCCESS,
    SEND_POST_ERROR,
    REMOVE_POST_SUCCESS,
    FETCH_SELECTED_POST_SUCCESS}
    from "../actions/actionTypes";

const initialState = {
    error: null,
    news: [],
    post:null
};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {
        case SEND_POST_ERROR:
             return {...state, error: action.error};
        case FETCH_NEWS_SUCCESS:
            return {...state, news: action.news};
        case FETCH_SELECTED_POST_SUCCESS:
            return {...state, post: action.post};
        case REMOVE_POST_SUCCESS:
            let copyStateNews = {...state.news};
            delete copyStateNews[action.id]
            return {
                ...state,
                news: {...copyStateNews}
            }
            default:
            return state;
    }
};

export default newsReducer;