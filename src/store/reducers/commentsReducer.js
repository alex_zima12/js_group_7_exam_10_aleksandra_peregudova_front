import {
    FETCH_COMMENTS_SUCCESS,
    // REMOVE_COMMENT_SUCCESS
} from "../actions/actionTypes";
const initialState = {
    error: null,
    comments: [],

};

const newsReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_COMMENTS_SUCCESS:
            return {...state, comments: action.comments};


        default:
            return state;
    }
};

export default newsReducer;