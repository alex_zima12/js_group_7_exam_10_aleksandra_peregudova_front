import {FETCH_COMMENTS_SUCCESS, REMOVE_COMMENT_SUCCESS, SEND_COMMENT_ERROR} from "./actionTypes";
import axios from "../../axiosApi";

const fetchCommentsSuccess = comments => {
    return {type: FETCH_COMMENTS_SUCCESS, comments}
};

export const fetchComments = (id) => {
    return async dispatch => {
        const response =  await axios.get('/?news_id=' + id);
        dispatch(fetchCommentsSuccess(response.data));
    };
};

const removeCommentSuccess = id => {
    return {type: REMOVE_COMMENT_SUCCESS, id}
};

export const removeComment = (id) => {
    return async dispatch => {
        await axios.delete('/comments/' + id);
        dispatch(removeCommentSuccess(id));
    };
};

const sendCommentError = error => {
    return {type: SEND_COMMENT_ERROR, error}
};

export const sendComment = commentData => {
    return async dispatch => {
        try {
            await axios.post("/comments", commentData)
        } catch (e) {
            console.log(e.response);
            dispatch(sendCommentError(e.response.data.error))
        }
    };
};