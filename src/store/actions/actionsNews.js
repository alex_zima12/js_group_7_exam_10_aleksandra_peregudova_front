import {FETCH_NEWS_SUCCESS,SEND_POST_ERROR,REMOVE_POST_SUCCESS,FETCH_SELECTED_POST_SUCCESS} from "./actionTypes";
import axios from "../../axiosApi";

const fetchNewsSuccess = (news) => {
    return {type: FETCH_NEWS_SUCCESS, news}
};

export const fetchNews = () => {
    return async dispatch => {
        const response = await axios.get("/news");
        dispatch(fetchNewsSuccess(response.data));
    };
};

const sendPostError = error => {
    return {type: SEND_POST_ERROR, error}
};

export const sendPost = messageData => {
    return async dispatch => {
        try {
            await axios.post("/news", messageData)
        } catch (e) {
            console.log(e.response);
            dispatch(sendPostError(e.response))
        }
    };
};

const removePostSuccess = id => {
    return {type: REMOVE_POST_SUCCESS, id}
};

export const removePost = (id) => {
    return async dispatch => {
        await axios.delete('/news/' + id);
        dispatch(removePostSuccess(id));
    };
};

const fetchSelectedPostSuccess = post => {
    return {type: FETCH_SELECTED_POST_SUCCESS, post}
};
export const fetchSelectedPost = (id) => {
    return async dispatch => {
        const response =  await axios.get('/news/' + id);
        dispatch(fetchSelectedPostSuccess(response.data));
    };
};
